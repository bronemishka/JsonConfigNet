using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using NUnit.Framework;
using JsonConfig;


namespace JConfigNUnitTest
{
    public class JsonTestClass : ConfigBase
    {
        public int SomeTestInt = 12;
    }

    public class Tests
    {
        private const string Path = "test.json";
        private const string PathDir = "test/test.json";

        [SetUp]
        public void Setup()
        {
            if (File.Exists(Path)) File.Delete(Path);
            if (Directory.Exists(PathDir)) Directory.Delete(System.IO.Path.GetDirectoryName(PathDir)!, true);
        }

        [Test]
        public void SaveJsonTest()
        {
            var cfg = new ConfigBuilder<JsonTestClass>()
                .ConfigPath(Path)
                .Build();
            cfg.SaveConfig();
            Assert.True(File.Exists(cfg.Path));
        }

        [Test]
        public void SaveInFolder()
        {
            var cfg = new ConfigBuilder<JsonTestClass>()
                .ConfigPath(PathDir)
                .Build();
            cfg.SaveConfig();
            Assert.True(File.Exists(cfg.Path));
        }

        [Test]
        public void LoadJsonTest()
        {
            var cfg = new ConfigBuilder<JsonTestClass>()
                .ConfigPath(Path)
                .Build();
            if (cfg.SomeTestInt == 12) Assert.Pass();
            else Assert.Fail();
        }
    }
}