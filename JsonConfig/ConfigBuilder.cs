﻿using System.Text.Json;
using Serilog;

namespace JsonConfig;

/// <summary>
/// Configuration main class.
/// </summary>
/// <typeparam name="T">Config template class</typeparam>
public class ConfigBuilder<T> where T : ConfigBase, new()
{
    private string Path { get; init; } = string.Empty;
    public ConfigBuilder<T> ConfigPath(string path) => new() { Path = path };

    public T Build()
    {
        var config = new T()
        {
            Path = Path
        };

        if (string.IsNullOrEmpty(config.Path))
        {
            throw new ArgumentException("You have to specify path using ConfigPath method");
        }

        Log.Information("Loading {Path}", config.Path);
        if (File.Exists(config.Path))
        {
            try
            {
                config = JsonSerializer.Deserialize<T>(File.ReadAllText(config.Path)) ??
                         throw new JsonException($"Can't load {config.Path}");
                config.Path = Path;
                Log.Debug("{Path} loaded.", config.Path);
            }
            catch (JsonException e)
            {
                Log.Error("{Path} file is broken.\n>>>{Message}", config.Path, e.Message);
            }
        }
        else
        {
            Log.Warning("{Path} not found, creating and saving default one", config.Path);
            config.SaveConfig();
        }

        return config;
    }
}