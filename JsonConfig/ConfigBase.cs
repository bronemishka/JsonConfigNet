﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Serilog;

namespace JsonConfig
{
    public abstract class ConfigBase
    {
        [JsonIgnore]
        private bool BuiltSuccessfully { get; set; }
        
        private string _path = string.Empty;

        [JsonIgnore]
        public string Path
        {
            get => _path;
            set
            {
                if (!BuiltSuccessfully)
                {
                    BuiltSuccessfully = true;
                }
                else
                {
                    throw new AccessViolationException("You cant change path of built config.");
                }

                _path = value;
            }
        }

        public void SaveConfig()
        {
            if (string.IsNullOrEmpty(Path))
            {
                throw new ArgumentException($"Path \"{Path}\" not found");
            }
            Log.Information("Saving {Path}", Path);
            CreateDirectories();
            File.WriteAllText(Path, JsonSerializer.Serialize(this));
            Log.Information("{Path} saved.", Path);
        }
        
        private void CreateDirectories()
        {
            var path = System.IO.Path.GetDirectoryName(Path);
            if (string.IsNullOrEmpty(path)) return;
            Log.Debug("Creating directories...");
            Directory.CreateDirectory(path);
        }
    }
}
