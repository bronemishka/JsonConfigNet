# JsonConfigNet
<img src="icon.png" alt="JsonConfigLogo" style="height: 256px; width:256px;"/>
<br>
Pretty simple library that allows easily managing and creating json configs.

## 📕Description
Main target of this library is to make simple library that can be used in any of your projects to simplify creation and managment of json configuration files
\
For that reason I created **this**.

## 🛠Installation
You can download nuget package [here](https://www.nuget.org/packages/JsonConfig-bronemishka/).
\
Otherwise you can compile library by yourself, and all you needed is .net6.0 or later and IDE, where you can open the solution or `dotnet build` if you know how to use it.

## 🏗Usage
To use this library firstly you need to compile it and include in your dependencies, or just install it as nuget package. Then import it by `using JsonConfig;` in your code

After this you can create an instance of **ConfigBuilder<T>**, and class that will inherit **ConfigBase** class. After creating instance of **ConfigBuilder<T>** you have to pass config pass there using **ConfigPath** method and then use **Build** method to create config. To save config you have to use **SaveConfig()**.

*Example of library usage can be found in tests [here](https://gitlab.com/bronemishka/JsonConfigNet/-/blob/main/JConfigNUnitTest/JsonTestClass.cs)

## 🆘Support
Feel free to suggest features and report bugs in issues tab. That will make this small lib much better

## 📝Contributing
If you want to contribute, then make sure that it scopes changes related to bugfixes and code improvements.

## 📃License
Licensed under GNU GPL v3.0